#ifndef OUTPUT_H
#define OUTPUT_H

#include <QDrag>
#include <QListWidget>
#include <QMimeData>
#include <QMouseEvent>
#include <QString>
#include <iostream>
#include <sstream>

#include "./CustomGraphicsView.h"
#include "./headers/Node.h"

class CustomGraphicsView;
class Input;

class Output : public QListWidget {
private:
  Input *next;
  QPoint offset;
  QChar color;
  QString addrtos(QWidget *w);

public:
  explicit Output(QWidget *parent = nullptr);
  QChar getColor();
  void setColor(QChar c);
  Input *getNext();
  void setNext(Input *ptr);

protected:
  void startDrag(Qt::DropActions supportedActions) override;
  // void mousePressEvent(QMouseEvent *event) override;
  // void mouseMoveEvent(QMouseEvent *event) override;
  // void mouseReleaseEvent(QMouseEvent *event) override;
};

#endif // OUTPUT_H
