#ifndef VARNODE_H
#define VARNODE_H

#include <QComboBox>
#include <QGridLayout>
#include <QLineEdit>

#include "./headers/Node.h"

class VarNode : public Node {
public:
  VarNode();
  QLineEdit *value;
  QLineEdit *varName;
  QComboBox *combo;
  QListWidgetItem *listItem;

  QString getCodeForNode() override;
  QString getVarName() const override;
  QLineEdit *getVarName();
  QListWidgetItem *getListItem();
};

#endif // VARNODE_H
