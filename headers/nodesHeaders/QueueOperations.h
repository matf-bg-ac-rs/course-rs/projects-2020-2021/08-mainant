#ifndef QUEUEOPERATIONS_H
#define QUEUEOPERATIONS_H

#include <QComboBox>
#include <QGridLayout>
#include <QLineEdit>

#include "./headers/Node.h"

class QueueOperations : public Node {
public:
  QueueOperations();

  QLineEdit *value;
  QComboBox *operations;

  QString getCodeForNode() override;
};

#endif // QUEUEOPERATIONS_H
