#ifndef SRANDNODE_H
#define SRANDNODE_H

#include "./headers/Node.h"
#include <QString>

class SrandNode : public Node {
public:
  SrandNode();
  virtual QString getCodeForNode() override;
};

#endif // SRANDNODE_H
