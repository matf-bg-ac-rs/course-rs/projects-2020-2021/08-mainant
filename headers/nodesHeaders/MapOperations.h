#ifndef MAPOPERATIONS_H
#define MAPOPERATIONS_H

#include <QComboBox>
#include <QGridLayout>
#include <QLineEdit>

#include "./headers/Node.h"

class MapOperations : public Node {
public:
  MapOperations();

  QLineEdit *value;
  QComboBox *operations;

  QString getCodeForNode() override;
};

#endif // MAPOPERATIONS_H
