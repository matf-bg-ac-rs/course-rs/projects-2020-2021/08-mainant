#ifndef OUTPUTNODE_H
#define OUTPUTNODE_H

#include "./headers/Node.h"
#include <QApplication>
#include <QClipboard>
#include <QFont>
#include <QFormLayout>
#include <QLineEdit>
#include <QString>
#include <QTextEdit>
#include <iostream>

class InputNode : public Node {
private:
  QString text = "";
  QLineEdit *input;

public:
  InputNode();
  QString getCodeForNode() override;
  QString getText() const;
};

#endif // OUTPUTNODE_H
