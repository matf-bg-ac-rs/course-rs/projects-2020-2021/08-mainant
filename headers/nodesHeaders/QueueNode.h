#ifndef QUEUENODE_H
#define QUEUENODE_H

#include <QButtonGroup>
#include <QComboBox>
#include <QGridLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QRadioButton>
#include <QVector>

#include "./headers/Node.h"

class QueueNode : public Node {
  Q_OBJECT
public:
  QueueNode(QVector<QString> &_inicializedClases);

  QLineEdit *QueueName;
  QComboBox *varTypes;
  QLineEdit *capacity;
  QVector<QString> initializedVars;

  QLineEdit *in;
  QLineEdit *lastFive;
  QPushButton *addVar;
  QPushButton *removeVar;
  QString lastFiveText = "empty queue";
  QButtonGroup *choice;
  QRadioButton *initialize;
  QRadioButton *notInitialize;

  int initialSize = 220;

  QString getCodeForNode() override;
  QString getVarName() const override;
  void genererateLastFiveText();

public slots:
  void addVariable();
  void removeVariable();
};

#endif // QUEUENODE_H
