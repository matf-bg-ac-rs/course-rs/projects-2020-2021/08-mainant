#ifndef WHILENODE_H
#define WHILENODE_H

#include <QFont>
#include <QString>
#include <QTextEdit>

#include "./BodyNode.h"
#include "./headers/Node.h"
#include "./headers/nodesHeaders/ConditionNode.h"

class WhileNode : public Node {
public:
  WhileNode();

  QString getCodeForNode() override;
};

#endif // WHILENODE_H
