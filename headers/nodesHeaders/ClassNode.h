#ifndef CLASSNODE_H
#define CLASSNODE_H

#include <QComboBox>
#include <QGridLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QVector>

#include "./headers/FunctionWindow.h"
#include "./headers/Node.h"
#include "./headers/nodesHeaders/ClassField.h"

class ClassNode : public Node {
  Q_OBJECT
private:
  int methodId;
  int classId;

public:
  ClassNode(int classNum);
  QLineEdit *ClassName;
  QPushButton *addMethod;
  QPushButton *addVariable;
  QVector<ClassField *> fields;
  QVector<QString> definedAttributes;
  int getMethodId();

  // Public metoddi klase
  QVector<QString> publicMethods;
  // private metodi klase
  QVector<QString> privateMethods;
  // protected methodi klase
  QVector<QString> protectedMethods;

  QString getCodeForNode() override;
public slots:
  void addMethodSlot();
  void addFieldSlot();
  void fillDefinedAttributes();
  QVector<ClassField *> &getAttributes();
};

#endif // CLASSNODE_H
