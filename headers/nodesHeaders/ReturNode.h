#ifndef RETURNNODE_H
#define RETURNNODE_H

#include "./headers/Node.h"
#include <QButtonGroup>
#include <QLineEdit>
#include <QRadioButton>
#include <iostream>

class ReturnNode : public Node {
private:
  bool earlyEnd;
  QLineEdit *returncCode;

public:
  ReturnNode();
  virtual QString getCodeForNode() override;
};

#endif // RETURNNODE_H
