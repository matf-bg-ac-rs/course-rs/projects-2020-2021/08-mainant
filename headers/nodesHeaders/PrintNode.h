#ifndef PRINTNODE_H
#define PRINTNODE_H

#include "./headers/Node.h"
#include <QAbstractButton>
#include <QButtonGroup>
#include <QFont>
#include <QFormLayout>
#include <QRadioButton>
#include <QString>
#include <QTextEdit>

class PrintNode : public Node {
private:
  bool manualInput;
  bool fileInput;
  bool printMyInput;
  QTextEdit *input;

public:
  PrintNode();
  QString getCodeForNode() override;
};

#endif // PRINTNODE_H
