#ifndef VECTOROPERATIONS_H
#define VECTOROPERATIONS_H

#include <QComboBox>
#include <QGridLayout>
#include <QLineEdit>

#include "./headers/Node.h"

class VectorOperations : public Node {
public:
  VectorOperations();

  QLineEdit *value;
  QComboBox *operations;

  QString getCodeForNode() override;
};

#endif // VECTOROPERATIONS_H
